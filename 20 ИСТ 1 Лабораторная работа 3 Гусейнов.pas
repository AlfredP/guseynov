type
  student = class
  public
    LastName: string;
    Points: integer;
    
    constructor(name: string; ball: integer);
    begin
      LastName := name;  
      Points := ball; 
    end;
  end;

const
  n = 10;

var
  journal: array[1..n] of student;
  percent5: single := 0.2;
  percent4: single := 0.3;
  percent3: single := 0.4;

begin
  
  write('Сохранить в файле результаты тестирования абитуриентов по некоторому предмету. Упорядочить список по баллам. Первым k1% студентам поставить оценку 5, следующим k2% - поставить 4, k3% студентами поставить 3, оставшимся - 2 балла. Структура информации: фамилия, количество баллов, оценка.');
  
  journal[1] := new student('Иванов', 80); 
  journal[2] := new student('Петров', 96); 
  journal[3] := new student('Сидоров', 92);
  journal[4] := new student('Калушкин', 41);
  journal[5] := new student('Быстров', 30);
  journal[6] := new student('Костров', 55);
  journal[7] := new student('Кисляков', 67);
  journal[8] := new student('Осетров', 42);
  journal[9] := new student('Губо', 15);
  journal[10] := new student('Пигалин', 22);
  
  var k: student; 
  var i, j: integer;
  var Ocenki: text;
  Assign(Ocenki, 'Ocenki.txt');
  Rewrite(Ocenki);
  Writeln(Ocenki, 'Студент  Баллы  Оценка');
  
  for i := 1 to n - 1 do 
  begin
    for j := i + 1 to n do  
    begin
      if (journal[i].Points < journal[j].Points) then 
      begin
        k := journal[i];
        journal[i] := journal[j];
        journal[j] := k;
      end
    end;
  end;
  
  for i := 1 to n do
  begin
    if (i <= n * percent5) then
      Writeln(Ocenki, journal[i].LastName, ' ', journal[i].Points, ' ', 5)
    else if (i <= n * (percent4 + percent5)) then
      Writeln(Ocenki, journal[i].LastName, ' ', journal[i].Points, ' ', 4)
    else if (i <= n * (percent3 + percent4 + percent5)) then
      Writeln(Ocenki, journal[i].LastName, ' ', journal[i].Points, ' ', 3)
    else 
      Writeln(Ocenki, journal[i].LastName, ' ', journal[i].Points, ' ', 2);  
  end;
  
  close(Ocenki);
end.

